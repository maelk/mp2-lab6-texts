﻿#include "gtest.h"
#include "TText.h"


TEST(TText, can_create_text_without_having_textlink) {

	TTextLink::InitMemSystem();

	ASSERT_NO_THROW(TText text);
}

TEST(TText, can_create_text_without_having_textlink_specifying_memory) {

	TTextLink::InitMemSystem(3);

	ASSERT_NO_THROW(TText text);
}

TEST(TText, can_create_text_from_textlink) {
	
	TTextLink::InitMemSystem();
	TTextLink* link;

	link = new TTextLink();

	ASSERT_NO_THROW(TText text(link));
}

TEST(TText, can_copy_text) {
	
	TTextLink::InitMemSystem();
	TText text;

	ASSERT_NO_THROW(text.GetCopy());
}

TEST(TText, can_go_first_link) {
	
	TTextLink::InitMemSystem();
	TText text;

	ASSERT_NO_THROW(text.GoFirstLink());
}

TEST(TText, can_go_down_link)
{
	const string str1 = "str1", str2 = "str2";
	TTextLink::InitMemSystem(2);
	TText text;

	text.GoFirstLink();
	text.SetLine(str1);
	text.InsDownLine(str2);

	ASSERT_NO_THROW(text.GoDownLink());
}

TEST(TText, go_down_link_works_correctly)
{
	const string str1 = "string1", str2 = "string2";
	TTextLink::InitMemSystem(2);
	TText text;

	text.GoFirstLink();
	text.SetLine(str1);
	text.InsDownLine(str2);
	text.GoDownLink();

	ASSERT_EQ(text.GetLine(), str2);
}

TEST(TText, can_go_next_link)
{
	const string str1 = "string1", str2 = "string2";
	TTextLink::InitMemSystem(2);
	TText text;

	text.GoFirstLink();
	text.SetLine(str1);
	text.InsNextLine(str2);

	ASSERT_NO_THROW(text.GoNextLink());
}

TEST(TText, go_next_link_works_correctly)
{
	const string str1 = "string1", str2 = "string2";
	TTextLink::InitMemSystem(2);
	TText text;

	text.GoFirstLink();
	text.SetLine(str1);
	text.InsNextLine(str2);
	text.GoNextLink();

	ASSERT_EQ(text.GetLine(), str2);
}

TEST(TText, can_go_prev_link)
{
	const string str1 = "string1", str2 = "string2";
	TTextLink::InitMemSystem(2);
	TText text;

	text.GoFirstLink();
	text.SetLine(str1);
	text.InsNextLine(str2);

	ASSERT_NO_THROW(text.GoPrevLink());
}

TEST(TText, go_prev_link_works_correctly)
{
	const string str1 = "string1", str2 = "str2ing";
	TTextLink::InitMemSystem(2);
	TText text;

	text.GoFirstLink();
	text.SetLine(str1);
	text.InsNextLine(str2);
	text.GoNextLink();
	text.GoPrevLink();

	ASSERT_EQ(text.GetLine(), str1);
}

TEST(TText, can_add_down_a_line) {
	
	TTextLink::InitMemSystem();
	TText text;
	std::string str;

	str = "test";
	text.GoFirstLink();
	text.InsDownLine(str);
	text.GoDownLink();

	EXPECT_EQ(text.GetLine(), str);
}

TEST(TText, can_ins_down_section)
{
	const string str1 = "string1", str2 = "string2";
	TTextLink::InitMemSystem(2);
	TText text;

	text.GoFirstLink();
	text.SetLine(str1);

	ASSERT_NO_THROW(text.InsDownSection(str2));
}

TEST(TText, can_ins_next_line)
{
	const string str1 = "string1", str2 = "string2";
	TTextLink::InitMemSystem(2);
	TText text;

	text.GoFirstLink();
	text.SetLine(str1);

	ASSERT_NO_THROW(text.InsNextLine(str2));
}

TEST(TText, can_ins_next_section)
{
	const string str1 = "string1", str2 = "string2";
	TTextLink::InitMemSystem(2);
	TText text;

	text.GoFirstLink();
	text.SetLine(str1);

	ASSERT_NO_THROW(text.InsNextSection(str2));
}

TEST(TText, can_reset_iterator) {
	
	TTextLink::InitMemSystem();
	TText text;

	ASSERT_NO_THROW(text.Reset());
}

TEST(TText, can_check_if_text_ended) {

	TTextLink::InitMemSystem();
	TText text;

	ASSERT_NO_THROW(text.IsTextEnded());
}

TEST(TText, can_iterate) {
	
	TTextLink::InitMemSystem();
	TText text;

	ASSERT_NO_THROW(text.GoNext());
}

TEST(TText, can_write_to_file) {
	
	TTextLink::InitMemSystem();
	TText text;
	char* filename;

	filename = "__test_text_out.txt";

	ASSERT_NO_THROW(text.Write(filename));
}

TEST(TText, can_print_text) {

	TTextLink::InitMemSystem();
	TText text;

	ASSERT_NO_THROW(text.Print());
}

TEST(TText, can_del_down_line)
{
	const string str1 = "string1", str2 = "string2";
	TTextLink::InitMemSystem(2);
	TText text;

	text.GoFirstLink();
	text.SetLine(str1);
	text.InsDownLine(str2);

	ASSERT_NO_THROW(text.DelDownLine());
}

TEST(TText, can_del_down_section)
{
	const string str1 = "string1", str2 = "string2";
	TTextLink::InitMemSystem(2);
	TText text;

	text.GoFirstLink();
	text.SetLine(str1);
	text.InsDownSection(str2);

	ASSERT_NO_THROW(text.DelDownSection());
}

TEST(TText, can_del_next_line)
{
	const string str1 = "string1", str2 = "string2";
	TTextLink::InitMemSystem(2);
	TText text;

	text.GoFirstLink();
	text.SetLine(str1);
	text.InsNextLine(str2);

	ASSERT_NO_THROW(text.DelNextLine());
}

TEST(TText, can_del_next_section)
{
	const string str1 = "string1", str2 = "string2";
	TTextLink::InitMemSystem(2);
	TText text;

	text.GoFirstLink();
	text.SetLine(str1);
	text.InsNextSection(str2);

	ASSERT_NO_THROW(text.DelNextSection());
}