﻿# Лабораторная работа №6: Тексты

## Введение ##

Обработка текстовой информации на компьютере широко применяется в различных областях человеческой деятельности: образование, наука, документооборот, кадровый и бухгалтерский учет и др. Вне зависимости от назначения текста типовыми операциями обработки являются создание, просмотр, редактирование и сохранение информации. В связи с тем, что объем текстовой информации может являться очень большим, для эффективного выполнения операций с ней необходимо выбрать представление текста, обеспечивающее структурирование и быстрый доступ к различным элементам текста. Так, текст можно представить в виде линейной последовательности страниц, каждая из которых есть линейная последовательность строк, которые  в свою очередь являются линейными последовательностями слов. Такое представление можно осуществлять с любой степенью детализации в зависимости от особенностей прикладной задачи.В рамках лабораторной работы рассматривается задача разработки учебного редактора текстов, в котором для представления данных используется иерархический связный список. Подобная иерархическая структура представления может применяться при компьютерной реализации математических моделей в виде деревьев и, тем самым, может иметь самое широкое применение в самых различных областях приложений. ##  Основные понятия и определения

**Текст** – это несколько предложений, связанных друг с другом по смыслу и грамматически. В рамках лабораторной работы в качестве примеров текстов рассматриваются тексты программ.**Редактор текстов** – программный комплекс, обеспечивающий выполнение операций обработки текста: создание, просмотр, редактирование и сохранение. Специализированные редакторы текстов могут поддерживать выполнение дополнительных операций (например, проверку синтаксиса или контекстный поиск). **Иерархический связный список** – это многосвязный список, в котором на каждое звено имеется ровно один указатель, а каждое звено содержит два указателя (один на следующее звено в том же уровне, другой на звено в нижерасположенном уровне).

## Цели и задачи

В рамках лабораторной работы ставится задача разработки учебного редактора текстов с поддержкой следующих операций:
* выбор текста для редактирования (или создание нового текста);
* демонстрация текста на экране дисплея;
* поддержка средств указания элементов (уровней) текста;
* вставка, удаление и замена строк текста;
* запись подготовленного текста в файл.

Была предложена следующая структура хранения текста:

![](classes.png)

Она подразумевает реализацию двух основных классов, `TTextLink` для хранения строк, и `TText` для хранения собственно текста.

Более точное описание структуры текста:

* каждое звено структуры хранения содержит два поля указателей и поле для хранения данных;
* нижний уровень иерархической структуры звеньев ограничивается
* уровнем строк, а не символов. Это повышает эффективность использования памяти, так как в случае отдельного уровня для хранения символов, затраты на хранение служебной информации (указатели) в несколько раз превышают затраты на хранение самих данных.При использовании строк в качестве атомарных элементов поле для хранения данных является массивом символов заданного размера;
* количество уровней и количество звеньев на каждом уровне может быть произвольным;
* при использовании звена на атомарном уровне поле указателя на нижний уровень структуры устанавливается равным значению NULL;
* при использовании звена на структурных уровнях представления текста поле для хранения строки текста используется для размещения наименования соответствующего элемента текста;
* количество используемых уровней представления в разных фрагментах текста может быть различным.

### Условия и ограничения
При выполнении лабораторной работы использовались следующие основные предположения:
1. При планировании структуры текста в качестве самого нижнего уровня можно рассматривать уровень строк.
2. В качестве тестовых текстов можно рассматривать текстовые файлы программы.

### План работы
1. Разработка структуры хранения текстов.
2. Разработка методов для доступа к строкам текста.
3. Возможность модификации структуры текста.
4. Создание механизма для обхода текста.
5. Реализация итератора.
6. Копирование текста.
7. Реализация сборки мусора.
8. Тестирование.


## Выполнение работы

### 1. TTextLink

Класс, реализующий строку текста `TTextLink`, наследует от базового абстрактного класса `TDatValue`.

### Класс TDatValue - абстрактный класс для объектов-значений списка
```C++
class TDatValue {
public:
    virtual TDatValue* getCopy() = 0;
    ~TDatValue() {};
};
```
В данном классе реализовано не только очевидное хранение данных текущей строки, но также алгоритм сборки мусора с неразрывно связанной специальной системой выделения памяти.

Алгоритм сборки мусора состоит из трех этапов.
1. Обход текста и маркирование звеньев текста специальными символами (например, «&&&»).
2. Обход и маркирование списка свободных звеньев.
3. Проход по всему непрерывному участку памяти как по непрерывному набору звеньев. Если звено промаркировано, то маркер снимается. В противном случае, найдено звено, на которое нет ссылок («мусор»), и это звено возвращается в список свободных звеньев.

Ещё одна сложность также состоит в том, что нам приходится по-особому выделять память для новых строк, а значит, нужно перегружать операторы `new` и `delete`.

Для выделения памяти под звено перегружается оператор `new`, который выделяет новое звено из списка свободных звеньев. При освобождении звена в перегруженном операторе `delete` происходит возвращение памяти в список свободных звеньев.

TTextLink.h:

```C++
#ifndef _TEXTLINK_H_
#define _TEXTLINK_H_

#include "TDatValue.h"
 #include <iostream>
 #include <string>

#define TextLineLength 30
#define MemSize 30

using namespace std;

class TText;
class TTextLink;
typedef TTextLink *PTTextLink;
typedef char TStr[TextLineLength];
class TTextMem {
	PTTextLink pFirst;     // указатель на первое звено
	PTTextLink pLast;      // указатель на последнее звено
	PTTextLink pFree;      // указатель на первое свободное звено
	friend class TTextLink;
};
typedef TTextMem *PTTextMem;

class TTextLink : public TDatValue {
protected:
	TStr Str;  // поле для хранения строки текста
	PTTextLink pNext, pDown;  // указатели по тек. уровень и на подуровень
	static TTextMem MemHeader; // система управления памятью
public:
	static void InitMemSystem(int size = MemSize); // инициализация памяти
	static void PrintFreeLink(void);  // печать свободных звеньев
	void * operator new (size_t size); // выделение звена
	void operator delete (void *pM);   // освобождение звена
	static void MemCleaner(TText &txt); // сборка мусора
	TTextLink(TStr s = NULL, PTTextLink pn = NULL, PTTextLink pd = NULL) {
		pNext = pn; pDown = pd;
		if (s != NULL) strcpy(Str, s); else Str[0] = '\0';
	}
	TTextLink(string s)
	{
		pNext = nullptr;
		pDown = nullptr;
		strcpy_s(Str, s.c_str());
	}
	~TTextLink() {}
	int IsAtom() { return pDown == NULL; } // проверка атомарности звена
	PTTextLink GetNext() { return pNext; }
	PTTextLink GetDown() { return pDown; }
	PTDatValue GetCopy() { return new TTextLink(Str, pNext, pDown); }
protected:
	virtual void Print(ostream &os) { os << Str; }
	friend class TText;
};

#endif
```

TTextLink.cpp:

```C++
#include "TTextLink.h"
#include "TText.h"

using namespace std;
TTextMem TTextLink::MemHeader;

void TTextLink::InitMemSystem(int size) { // инициализация памяти
	MemHeader.pFirst = (PTTextLink) new char[sizeof(TTextLink)* size]; 
	MemHeader.pFree = MemHeader.pFirst;
	MemHeader.pLast = MemHeader.pFirst + (size - 1);
	PTTextLink pLink = MemHeader.pFirst;
	for (int i = 0; i < size - 1; i++, pLink++) { // размерка памяти 
		pLink->pNext = pLink + 1;
	}
	pLink->pNext = nullptr;
}

void TTextLink::PrintFreeLink(void) { // печать свободных звеньев
	PTTextLink pLink = MemHeader.pFree;
	for (; pLink != nullptr; pLink = pLink->pNext)
	 cout << pLink->Str << endl;
}

void * TTextLink::operator new(size_t size) { // выделение звена
	PTTextLink pLink = MemHeader.pFree;
	if (MemHeader.pFree != nullptr)
		MemHeader.pFree = pLink->pNext;
	return pLink;
}
    
void TTextLink::operator delete(void *pM) { // освобождение звена
	PTTextLink pLink = (PTTextLink)pM;
	pLink->pNext = MemHeader.pFree;
	MemHeader.pFree = pLink;
}

void TTextLink::MemCleaner(TText &txt) { // сборка мусора
	for (txt.Reset(); !txt.IsTextEnded(); txt.GoNext()) {
		txt.SetLine("&&&" + txt.GetLine());
	}
	PTTextLink pLink = MemHeader.pFree;
	for (; pLink != nullptr; pLink->pNext)
		strcpy_s(pLink->Str, "&&&");
	pLink = MemHeader.pFirst;
	for (; pLink <= MemHeader.pLast; pLink++) {
		if (strstr(pLink->Str, "&&&") != nullptr) {
			strcpy_s(pLink->Str, pLink->Str + 3);	
		}
		else delete pLink;
	}
}
```
	
### 2. TText

Сложность реализации данного класса состоит в большом количестве необходимых для работы с текстом методов. Это и вставка и удаление текста на текущем и подуровне, итерирование текста, печать, ввод и вывод, связанный с файлами, а также копирование.

Для копирования текста необходимо осуществить обход текста. Так как структура текста является нелинейной, то копирование производится за два прохода, при этом для навигации по исходному тексту и тексту копии используется один объединенный стек.

Первый проход производится при подъеме на строку из подуровня – для текущей строки выполняется:

* создание копии звена;
* заполнение в звене-копии поля указателя подуровня pDown (подуровень уже скопирован);
* запись в звене-копии в поле данных значения “Copy”, используемое как маркер для распознавания звена при попадании на него при втором проходе; предполагается, что в тексте данный маркер не встречается;
* запись в звене-копии в поле указателя следующего звена pNext указателя на звено-оригинал (для возможности последующего копирования текста исходной строки);
* запись указателя на звено-копию в стек. Второй проход производится при извлечении звена-копии из стека (распознается по маркеру “Copy”)– в этом случае необходимо выполнить:
* заполнение в звене-копии полей данных и указателя следующего звена;
* указатель на звено-копию запоминается в служебной переменной.


TText.h:

```C++
#ifndef _TTEXT_H_
#define _TTEXT_H_

#include <stack>
#include <string>
#include <fstream>
#include "TDataCom.h"
#include "TTextLink.h"

class TText;
typedef TText *PTText;

class TText : public TDataCom {
protected:
	PTTextLink pFirst;      // указатель корня дерева
	PTTextLink pCurrent;      // указатель текущей строки
	stack< PTTextLink > Path; // стек траектории движения по тексту
							  // (звено pCurrent в стек не включается)
	stack< PTTextLink > St;   // стек для итератора
	PTTextLink GetFirstAtom(PTTextLink pl); // поиск первого атома
	void PrintText(PTTextLink ptl);         // печать текста со звена ptl
	PTTextLink ReadText(ifstream &TxtFile); //чтение текста из файла
	void PrintTextFile(PTTextLink ptl, ofstream& TxtFile); // запись текста в файл
public:
	TText(PTTextLink pl = nullptr);
	~TText() { pFirst = nullptr; }
	PTText GetCopy();
	// навигация
	int GoFirstLink(void); // переход к первой строке
	int GoDownLink(void);  // переход к следующей строке по Down
	int GoNextLink(void);  // переход к следующей строке по Next
	int GoPrevLink(void);  // переход к предыдущей позиции в тексте
						   // доступ
	string GetLine(void);   // чтение текущей строки
	void SetLine(string s); // замена текущей строки 
							// модификация
	void InsDownLine(string s);    // вставка строки в подуровень
	void InsDownSection(string s); // вставка раздела в подуровень
	void InsNextLine(string s);    // вставка строки в том же уровне
	void InsNextSection(string s); // вставка раздела в том же уровне
	void DelDownLine(void);        // удаление строки в подуровне
	void DelDownSection(void);     // удаление раздела в подуровне
	void DelNextLine(void);        // удаление строки в том же уровне
	void DelNextSection(void);     // удаление раздела в том же уровне
								   // итератор
	int Reset(void);              // установить на первую запись
	int IsTextEnded(void) const;  // текст завершен?
	int GoNext(void);             // переход к следующей записи
								  //работа с файлами
	void Read(string pFileName);  // ввод текста из файла
	void Write(string pFileName); // вывод текста в файл
								  //печать
	void Print(void);             // печать текста
};

#endif

```

TText.cpp:

```C++
// TText.cpp
// Тексты - иерархическая структура представления

#include "TText.h"

static int TextLevel; // номер текущего уровня текста

TText::TText(PTTextLink pl) {
	if (pl == nullptr)
		pl = new TTextLink;
	pFirst = pl;
}

// навигация
int TText::GoFirstLink(void) { // переход к первой строке
	while (!Path.empty())
		Path.pop();
	pCurrent = pFirst;
	if (pCurrent == nullptr)
		SetRetCode(TextError);
	else
		SetRetCode(TextOK);
	return RetCode;
}

int TText::GoDownLink(void) { //  переход к следующей строке по Down
	SetRetCode(TextNoDown);
	if (pCurrent != nullptr)
		if (pCurrent->pDown != nullptr) {
			Path.push(pCurrent);
			pCurrent = pCurrent->pDown;
			SetRetCode(TextOK);
		}
	return RetCode;
}

int TText::GoNextLink(void) { // переход к следующей строке по Next
	SetRetCode(TextNoNext);
	if (pCurrent != nullptr)
		if (pCurrent->pNext != nullptr) {
			Path.push(pCurrent);
			pCurrent = pCurrent->pNext;
			SetRetCode(TextOK);
		}
	return RetCode;
}

int TText::GoPrevLink(void) {
	if (Path.empty())
		SetRetCode(TextNoPrev);
	else {
		pCurrent = Path.top();
		Path.pop();
		SetRetCode(TextOK);
	}
	return RetCode;
}

// доступ
string TText::GetLine(void) { // чтение текущей строки
	if (pCurrent == nullptr) {
		SetRetCode(TextError);
		return "";
	}
	else {
		return string(pCurrent->Str);
	}
}

void TText::SetLine(string s) { // замена текущей строки
	if (pCurrent == nullptr)
		SetRetCode(TextError);
	else
		strcpy_s(pCurrent->Str, s.c_str());
}

// модификация
void TText::InsDownLine(string s) { // вставка строки в подуровень
	if (pCurrent == nullptr)
		SetRetCode(TextError);
	else {
		TStr buf;
		strcpy_s(buf, s.c_str());
		pCurrent->pDown = new TTextLink(buf, pCurrent->pDown, nullptr);
		SetRetCode(TextOK);
	}
}

void TText::InsDownSection(string s) { // вставка раздела в подуровень
	if (pCurrent == nullptr)
		SetRetCode(TextError);
	else {
		TStr buf;
		strcpy_s(buf, s.c_str());
		pCurrent->pDown = new TTextLink(buf, nullptr, pCurrent->pDown);
		SetRetCode(TextOK);
	}
}

void TText::InsNextLine(string s) { // вставка строки в том же уровне
	if (pCurrent == nullptr)
		SetRetCode(TextError);
	else {
		TStr buf;
		strcpy_s(buf, s.c_str());
		pCurrent->pNext = new TTextLink(buf, pCurrent->pNext, nullptr);
		SetRetCode(TextOK);
	}
}

void TText::InsNextSection(string s) { // вставка раздела в том же уровне
	if (pCurrent == nullptr)
		SetRetCode(TextError);
	else {
		TStr buf;
		strcpy_s(buf, s.c_str());
		pCurrent->pNext = new TTextLink(buf, nullptr, pCurrent->pNext);
		SetRetCode(TextOK);
	}
}

void TText::DelDownLine(void) { // удаление строки в подуровне
	SetRetCode(TextOK);
	if (pCurrent == nullptr)
		SetRetCode(TextError);
	else if (pCurrent->pDown == nullptr)
		SetRetCode(TextNoDown);
	else if (pCurrent->pDown->IsAtom())
		pCurrent->pDown = pCurrent->pDown->pNext;
}

void TText::DelDownSection(void) { // удаление раздела в подуровне 
	SetRetCode(TextOK);
	if (pCurrent == nullptr)
		SetRetCode(TextError);
	else if (pCurrent->pDown == nullptr)
		SetRetCode(TextNoDown);
	else {
		pCurrent->pDown = nullptr;
	}
}

void TText::DelNextLine(void) { // удаление строки в том же уровне
	SetRetCode(TextOK);
	if (pCurrent == nullptr)
		SetRetCode(TextError);
	else if (pCurrent->pNext == nullptr)
		SetRetCode(TextNoNext);
	else if (pCurrent->pNext->IsAtom())
		pCurrent->pNext = pCurrent->pNext->pNext;
}

void TText::DelNextSection(void) { // удаление раздела в том же уровне
	SetRetCode(TextOK);
	if (pCurrent == nullptr)
		SetRetCode(TextError);
	else if (pCurrent->pNext == nullptr)
		SetRetCode(TextNoNext);
	else
		pCurrent->pNext = pCurrent->pNext->pNext;
}

// итератор
int TText::Reset(void) { // установить на первую запись
	while (!St.empty())
		St.pop();
	pCurrent = pFirst;
	if (pCurrent != nullptr) {
		St.push(pCurrent);
		if (pCurrent->pNext != nullptr)
			St.push(pCurrent->pNext);
		if (pCurrent->pDown != nullptr)
			St.push(pCurrent->pDown);
	}
	return IsTextEnded();
}

int TText::IsTextEnded(void) const { // текст завершен?
	return !St.size();
}

int TText::GoNext(void) { // Переход к следующей записи
	if (!IsTextEnded()) {
		pCurrent = St.top(); St.pop();
		if (pCurrent != pFirst) {
			if (pCurrent->pNext != nullptr)
				St.push(pCurrent->pNext);
			if (pCurrent->pDown != nullptr)
				St.push(pCurrent->pDown);
		}
	}
	return IsTextEnded();
}

// копирование текста
PTTextLink TText::GetFirstAtom(PTTextLink pl) { // поиск первого атома
	PTTextLink tmp = pl;
	while (!tmp->IsAtom()) {
		St.push(tmp);
		tmp = tmp->GetDown();
	}
	return tmp;
}

PTText TText::GetCopy() { // копирование текста
	PTTextLink pl1, pl2, pl = pFirst, cpl = nullptr;

	if (pFirst != nullptr) {
		while (!St.empty())
			St.pop(); // очистка стека
		while (true) {
			if (pl != nullptr) { // переход к первому атому
				pl = GetFirstAtom(pl);
				St.push(pl);
				pl = pl->GetDown();
			}
			else if (St.empty()) break;
			else {
				pl1 = St.top(); St.pop();
				if (strstr(pl1->Str, "Copy") == nullptr) { // первый этап создания копии
														   // создание копии - pDown на уже скопированный подуровень
					pl2 = new TTextLink("Copy", pl1, cpl); // pNext на оригинал
					St.push(pl2);
					pl = pl1->GetNext();
					cpl = nullptr;
				}
				else { // второй этап создания копии
					pl2 = pl1->GetNext();
					strcpy_s(pl1->Str, pl2->Str);
					pl1->pNext = cpl;
					cpl = pl1;
				}
			}
		}
	}
	return new TText(cpl);
}

// печать текста
void TText::Print() {
	TextLevel = 0;
	PrintText(pFirst);
	Reset();
}

void TText::PrintText(PTTextLink ptl) {
	if (ptl != nullptr) {
		for (int i = 0; i < TextLevel; i++)
			cout << " ";
		cout << ptl->Str << endl;
		TextLevel++; PrintText(ptl->GetDown());
		TextLevel--; PrintText(ptl->GetNext());
	}
}

void TText::PrintTextFile(PTTextLink ptl, ofstream& txtFile)
{
	if (ptl != nullptr) {
		for (int i = 0; i < TextLevel; i++)
			txtFile << " ";
		txtFile << ptl->Str << endl;
		TextLevel++; PrintTextFile(ptl->GetDown(), txtFile);
		TextLevel--; PrintTextFile(ptl->GetNext(), txtFile);
	}
}

void TText::Write(string pFileName)
{
	TextLevel = 0;
	ofstream TextFile(pFileName);
	PrintTextFile(pFirst, TextFile);
	Reset();
}

// чтение текста
void TText::Read(string pFileName) {
	ifstream txtFile(pFileName);
	TextLevel = 0;
	if (&txtFile != nullptr)
		pFirst = ReadText(txtFile);
	Reset();
}

PTTextLink TText::ReadText(ifstream& TxtFile)
{
	string buf;
	PTTextLink ptl = new TTextLink();
	PTTextLink tmp = ptl;
	while (!TxtFile.eof())
	{
		getline(TxtFile, buf);
		if (buf.front() == '}')
			break;
		else if (buf.front() == '{')
			ptl->pDown = ReadText(TxtFile);
		else
		{
			ptl->pNext = new TTextLink(buf.c_str());
			ptl = ptl->pNext;
		}
	}
	ptl = tmp;
	if (tmp->pDown == nullptr)
	{
		tmp = tmp->pNext;
		delete ptl;
	}
	return tmp;
}
```
	
### 3. Демонстрационная программа

TestProg.cpp:

```C++
#include "TText.h"

int main() {
    setlocale(LC_ALL, "Russian");
    PTText pText;
    TTextLink::InitMemSystem(100);
    TText text;
    string fileName;
	
    fileName = "C:\\Users\\А\\Desktop\\Новая папка\\input.txt";

    text.Read(fileName);
    cout << "Input text" << endl;
    text.Print();
    cout << endl;

    cout << "New text :\n";
    cout << endl;
    pText = text.GetCopy();
    pText->GoFirstLink();
    pText->SetLine("Методы программирования - 2");
    pText->InsDownLine("Раздел 2");
    pText->GoDownLink();
    pText->InsDownLine("2.1 Стэк");
    pText->GoDownLink();
    pText->InsDownLine("1. Описание и применение");
    pText->GoNextLink();
    pText->InsNextLine("2.2 Полиномы");
    pText->GoNextLink();
    pText->InsDownLine("1. Определение");
    pText->GoDownLink();
    pText->InsNextLine("2. Структура");
    pText->Print();
    fileName = "output.txt";
    pText->Write(fileName);
    cout << "\nModified text saved to file :" << fileName << endl;
    return 0;
}
```	


**Результат**	

![](samples.png)


### 4. Проверка работоспособности при помощи Google Test Framework

Данные классы были протестированы с помощью фреймворка **Google Test**.

test_text.cpp:

```C++
#include "gtest.h"
#include "TText.h"

TEST(TText, can_create_text_without_having_textlink) {

	TTextLink::InitMemSystem();

	ASSERT_NO_THROW(TText text);
}

TEST(TText, can_create_text_without_having_textlink_specifying_memory) {

	TTextLink::InitMemSystem(3);

	ASSERT_NO_THROW(TText text);
}

TEST(TText, can_create_text_from_textlink) {
	
	TTextLink::InitMemSystem();
	TTextLink* link;

	link = new TTextLink();

	ASSERT_NO_THROW(TText text(link));
}

TEST(TText, can_copy_text) {
	
	TTextLink::InitMemSystem();
	TText text;

	ASSERT_NO_THROW(text.GetCopy());
}

TEST(TText, can_go_first_link) {
	
	TTextLink::InitMemSystem();
	TText text;

	ASSERT_NO_THROW(text.GoFirstLink());
}

TEST(TText, can_go_down_link)
{
	const string str1 = "str1", str2 = "str2";
	TTextLink::InitMemSystem(2);
	TText text;

	text.GoFirstLink();
	text.SetLine(str1);
	text.InsDownLine(str2);

	ASSERT_NO_THROW(text.GoDownLink());
}

TEST(TText, go_down_link_works_correctly)
{
	const string str1 = "string1", str2 = "string2";
	TTextLink::InitMemSystem(2);
	TText text;

	text.GoFirstLink();
	text.SetLine(str1);
	text.InsDownLine(str2);
	text.GoDownLink();

	ASSERT_EQ(text.GetLine(), str2);
}

TEST(TText, can_go_next_link)
{
	const string str1 = "string1", str2 = "string2";
	TTextLink::InitMemSystem(2);
	TText text;

	text.GoFirstLink();
	text.SetLine(str1);
	text.InsNextLine(str2);

	ASSERT_NO_THROW(text.GoNextLink());
}

TEST(TText, go_next_link_works_correctly)
{
	const string str1 = "string1", str2 = "string2";
	TTextLink::InitMemSystem(2);
	TText text;

	text.GoFirstLink();
	text.SetLine(str1);
	text.InsNextLine(str2);
	text.GoNextLink();

	ASSERT_EQ(text.GetLine(), str2);
}

TEST(TText, can_go_prev_link)
{
	const string str1 = "string1", str2 = "string2";
	TTextLink::InitMemSystem(2);
	TText text;

	text.GoFirstLink();
	text.SetLine(str1);
	text.InsNextLine(str2);

	ASSERT_NO_THROW(text.GoPrevLink());
}

TEST(TText, go_prev_link_works_correctly)
{
	const string str1 = "string1", str2 = "string2";
	TTextLink::InitMemSystem(2);
	TText text;

	text.GoFirstLink();
	text.SetLine(str1);
	text.InsNextLine(str2);
	text.GoNextLink();
	text.GoPrevLink();

	ASSERT_EQ(text.GetLine(), str1);
}

TEST(TText, can_add_down_a_line) {
	
	TTextLink::InitMemSystem();
	TText text;
	std::string str;

	str = "test";
	text.GoFirstLink();
	text.InsDownLine(str);
	text.GoDownLink();

	EXPECT_EQ(text.GetLine(), str);
}

TEST(TText, can_ins_down_section)
{
	const string str1 = "string1", str2 = "string2";
	TTextLink::InitMemSystem(2);
	TText text;

	text.GoFirstLink();
	text.SetLine(str1);

	ASSERT_NO_THROW(text.InsDownSection(str2));
}

TEST(TText, can_ins_next_line)
{
	const string str1 = "string1", str2 = "string2";
	TTextLink::InitMemSystem(2);
	TText text;

	text.GoFirstLink();
	text.SetLine(str1);

	ASSERT_NO_THROW(text.InsNextLine(str2));
}

TEST(TText, can_ins_next_section)
{
	const string str1 = "string1", str2 = "string2";
	TTextLink::InitMemSystem(2);
	TText text;

	text.GoFirstLink();
	text.SetLine(str1);

	ASSERT_NO_THROW(text.InsNextSection(str2));
}

TEST(TText, can_reset_iterator) {
	
	TTextLink::InitMemSystem();
	TText text;

	ASSERT_NO_THROW(text.Reset());
}

TEST(TText, can_check_if_text_ended) {

	TTextLink::InitMemSystem();
	TText text;

	ASSERT_NO_THROW(text.IsTextEnded());
}

TEST(TText, can_iterate) {
	
	TTextLink::InitMemSystem();
	TText text;

	ASSERT_NO_THROW(text.GoNext());
}

TEST(TText, can_write_to_file) {
	
	TTextLink::InitMemSystem();
	TText text;
	char* filename;

	filename = "__test_text_out.txt";

	ASSERT_NO_THROW(text.Write(filename));
}

TEST(TText, can_print_text) {

	TTextLink::InitMemSystem();
	TText text;

	ASSERT_NO_THROW(text.Print());
}

TEST(TText, can_del_down_line)
{
	const string str1 = "string1", str2 = "string2";
	TTextLink::InitMemSystem(2);
	TText text;

	text.GoFirstLink();
	text.SetLine(str1);
	text.InsDownLine(str2);

	ASSERT_NO_THROW(text.DelDownLine());
}

TEST(TText, can_del_down_section)
{
	const string str1 = "string1", str2 = "string2";
	TTextLink::InitMemSystem(2);
	TText text;

	text.GoFirstLink();
	text.SetLine(str1);
	text.InsDownSection(str2);

	ASSERT_NO_THROW(text.DelDownSection());
}

TEST(TText, can_del_next_line)
{
	const string str1 = "string1", str2 = "string2";
	TTextLink::InitMemSystem(2);
	TText text;

	text.GoFirstLink();
	text.SetLine(str1);
	text.InsNextLine(str2);

	ASSERT_NO_THROW(text.DelNextLine());
}

TEST(TText, can_del_next_section)
{
	const string str1 = "string1", str2 = "string2";
	TTextLink::InitMemSystem(2);
	TText text;

	text.GoFirstLink();
	text.SetLine(str1);
	text.InsNextSection(str2);

	ASSERT_NO_THROW(text.DelNextSection());
}
```

	
**Результат**

![](tests.png)


	
## Вывод

В ходе выполнения данной работы были получены навыки создания структуры данных для хранения текста и организации работы с ним, включая доступ к элементам текста, модицикацию структуры текста, обход текста, копирование текста и т.д. 

Функциональность написанной системы была протестирована при помощи Google Test Framework. Тесты показали, что разработанная программа успешно решает поставленную в начале работы задачу.