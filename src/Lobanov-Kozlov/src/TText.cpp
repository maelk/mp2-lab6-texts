// TText.cpp
// ������ - ������������� ��������� �������������

#include "TText.h"

static int TextLevel; // ����� �������� ������ ������

TText::TText(PTTextLink pl) {
	if (pl == nullptr)
		pl = new TTextLink;
	pFirst = pl;
}

// ���������
int TText::GoFirstLink(void) { // ������� � ������ ������
	while (!Path.empty())
		Path.pop();
	pCurrent = pFirst;
	if (pCurrent == nullptr)
		SetRetCode(TextError);
	else
		SetRetCode(TextOK);
	return RetCode;
}

int TText::GoDownLink(void) { //  ������� � ��������� ������ �� Down
	SetRetCode(TextNoDown);
	if (pCurrent != nullptr)
		if (pCurrent->pDown != nullptr) {
			Path.push(pCurrent);
			pCurrent = pCurrent->pDown;
			SetRetCode(TextOK);
		}
	return RetCode;
}

int TText::GoNextLink(void) { // ������� � ��������� ������ �� Next
	SetRetCode(TextNoNext);
	if (pCurrent != nullptr)
		if (pCurrent->pNext != nullptr) {
			Path.push(pCurrent);
			pCurrent = pCurrent->pNext;
			SetRetCode(TextOK);
		}
	return RetCode;
}

int TText::GoPrevLink(void) {
	if (Path.empty())
		SetRetCode(TextNoPrev);
	else {
		pCurrent = Path.top();
		Path.pop();
		SetRetCode(TextOK);
	}
	return RetCode;
}

// ������
string TText::GetLine(void) { // ������ ������� ������
	if (pCurrent == nullptr) {
		SetRetCode(TextError);
		return "";
	}
	else {
		return string(pCurrent->Str);
	}
}

void TText::SetLine(string s) { // ������ ������� ������
	if (pCurrent == nullptr)
		SetRetCode(TextError);
	else
		strcpy_s(pCurrent->Str, s.c_str());
}

// �����������
void TText::InsDownLine(string s) { // ������� ������ � ����������
	if (pCurrent == nullptr)
		SetRetCode(TextError);
	else {
		TStr buf;
		strcpy_s(buf, s.c_str());
		pCurrent->pDown = new TTextLink(buf, pCurrent->pDown, nullptr);
		SetRetCode(TextOK);
	}
}

void TText::InsDownSection(string s) { // ������� ������� � ����������
	if (pCurrent == nullptr)
		SetRetCode(TextError);
	else {
		TStr buf;
		strcpy_s(buf, s.c_str());
		pCurrent->pDown = new TTextLink(buf, nullptr, pCurrent->pDown);
		SetRetCode(TextOK);
	}
}

void TText::InsNextLine(string s) { // ������� ������ � ��� �� ������
	if (pCurrent == nullptr)
		SetRetCode(TextError);
	else {
		TStr buf;
		strcpy_s(buf, s.c_str());
		pCurrent->pNext = new TTextLink(buf, pCurrent->pNext, nullptr);
		SetRetCode(TextOK);
	}
}

void TText::InsNextSection(string s) { // ������� ������� � ��� �� ������
	if (pCurrent == nullptr)
		SetRetCode(TextError);
	else {
		TStr buf;
		strcpy_s(buf, s.c_str());
		pCurrent->pNext = new TTextLink(buf, nullptr, pCurrent->pNext);
		SetRetCode(TextOK);
	}
}

void TText::DelDownLine(void) { // �������� ������ � ���������
	SetRetCode(TextOK);
	if (pCurrent == nullptr)
		SetRetCode(TextError);
	else if (pCurrent->pDown == nullptr)
		SetRetCode(TextNoDown);
	else if (pCurrent->pDown->IsAtom())
		pCurrent->pDown = pCurrent->pDown->pNext;
}

void TText::DelDownSection(void) { // �������� ������� � ��������� 
	SetRetCode(TextOK);
	if (pCurrent == nullptr)
		SetRetCode(TextError);
	else if (pCurrent->pDown == nullptr)
		SetRetCode(TextNoDown);
	else {
		pCurrent->pDown = nullptr;
	}
}

void TText::DelNextLine(void) { // �������� ������ � ��� �� ������
	SetRetCode(TextOK);
	if (pCurrent == nullptr)
		SetRetCode(TextError);
	else if (pCurrent->pNext == nullptr)
		SetRetCode(TextNoNext);
	else if (pCurrent->pNext->IsAtom())
		pCurrent->pNext = pCurrent->pNext->pNext;
}

void TText::DelNextSection(void) { // �������� ������� � ��� �� ������
	SetRetCode(TextOK);
	if (pCurrent == nullptr)
		SetRetCode(TextError);
	else if (pCurrent->pNext == nullptr)
		SetRetCode(TextNoNext);
	else
		pCurrent->pNext = pCurrent->pNext->pNext;
}

// ��������
int TText::Reset(void) { // ���������� �� ������ ������
	while (!St.empty())
		St.pop();
	pCurrent = pFirst;
	if (pCurrent != nullptr) {
		St.push(pCurrent);
		if (pCurrent->pNext != nullptr)
			St.push(pCurrent->pNext);
		if (pCurrent->pDown != nullptr)
			St.push(pCurrent->pDown);
	}
	return IsTextEnded();
}

int TText::IsTextEnded(void) const { // ����� ��������?
	return !St.size();
}

int TText::GoNext(void) { // ������� � ��������� ������
	if (!IsTextEnded()) {
		pCurrent = St.top(); St.pop();
		if (pCurrent != pFirst) {
			if (pCurrent->pNext != nullptr)
				St.push(pCurrent->pNext);
			if (pCurrent->pDown != nullptr)
				St.push(pCurrent->pDown);
		}
	}
	return IsTextEnded();
}

// ����������� ������
PTTextLink TText::GetFirstAtom(PTTextLink pl) { // ����� ������� �����
	PTTextLink tmp = pl;
	while (!tmp->IsAtom()) {
		St.push(tmp);
		tmp = tmp->GetDown();
	}
	return tmp;
}

PTText TText::GetCopy() { // ����������� ������
	PTTextLink pl1, pl2, pl = pFirst, cpl = nullptr;

	if (pFirst != nullptr) {
		while (!St.empty())
			St.pop(); // ������� �����
		while (true) {
			if (pl != nullptr) { // ������� � ������� �����
				pl = GetFirstAtom(pl);
				St.push(pl);
				pl = pl->GetDown();
			}
			else if (St.empty()) break;
			else {
				pl1 = St.top(); St.pop();
				if (strstr(pl1->Str, "Copy") == nullptr) { // ������ ���� �������� �����
														   // �������� ����� - pDown �� ��� ������������� ����������
					pl2 = new TTextLink("Copy", pl1, cpl); // pNext �� ��������
					St.push(pl2);
					pl = pl1->GetNext();
					cpl = nullptr;
				}
				else { // ������ ���� �������� �����
					pl2 = pl1->GetNext();
					strcpy_s(pl1->Str, pl2->Str);
					pl1->pNext = cpl;
					cpl = pl1;
				}
			}
		}
	}
	return new TText(cpl);
}

// ������ ������
void TText::Print() {
	TextLevel = 0;
	PrintText(pFirst);
	Reset();
}

void TText::PrintText(PTTextLink ptl) {
	if (ptl != nullptr) {
		for (int i = 0; i < TextLevel; i++)
			cout << " ";
		cout << ptl->Str << endl;
		TextLevel++; PrintText(ptl->GetDown());
		TextLevel--; PrintText(ptl->GetNext());
	}
}

void TText::PrintTextFile(PTTextLink ptl, ofstream& txtFile)
{
	if (ptl != nullptr) {
		for (int i = 0; i < TextLevel; i++)
			txtFile << " ";
		txtFile << ptl->Str << endl;
		TextLevel++; PrintTextFile(ptl->GetDown(), txtFile);
		TextLevel--; PrintTextFile(ptl->GetNext(), txtFile);
	}
}

void TText::Write(string pFileName)
{
	TextLevel = 0;
	ofstream TextFile(pFileName);
	PrintTextFile(pFirst, TextFile);
	Reset();
}

// ������ ������
void TText::Read(string pFileName) {
	ifstream txtFile(pFileName);
	TextLevel = 0;
	if (&txtFile != nullptr)
		pFirst = ReadText(txtFile);
	Reset();
}

PTTextLink TText::ReadText(ifstream& TxtFile)
{
	string buf;
	PTTextLink ptl = new TTextLink();
	PTTextLink tmp = ptl;
	while (!TxtFile.eof())
	{
		getline(TxtFile, buf);
		if (buf.front() == '}')
			break;
		else if (buf.front() == '{')
			ptl->pDown = ReadText(TxtFile);
		else
		{
			ptl->pNext = new TTextLink(buf.c_str());
			ptl = ptl->pNext;
		}
	}
	ptl = tmp;
	if (tmp->pDown == nullptr)
	{
		tmp = tmp->pNext;
		delete ptl;
	}
	return tmp;
}