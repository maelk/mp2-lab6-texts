#define _CRT_SECURE_NO_WARNINGS
#include "gtest.h"
#include "TText.h"

TEST(TestKit_TText, can_create_text) {
	TText T;
	
    EXPECT_EQ(T.GetRetCode(),TextOK);
}

TEST(TestKit_TText, can_create_text_with_param) {
	TStr str = "Test";

	TTextLink Link(str);
	PTTextLink pLink = &Link;

	TText T(pLink);

	EXPECT_EQ(T.GetRetCode(), TextOK);
}

TEST(TestKit_TText, cant_go_first_link_in_empty_text) {
	TText T;	

	T.GoFirstLink();	

	EXPECT_EQ(T.GetRetCode(), TextError);
}

TEST(TestKit_TText, cant_go_down_link_in_empty_text) {
	TText T;

	T.GoDownLink();

	EXPECT_EQ(T.GetRetCode(), TextError);
}

TEST(TestKit_TText, cant_go_next_link_in_empty_text) {
	TText T;

	T.GoNextLink();

	EXPECT_EQ(T.GetRetCode(), TextError);
}

TEST(TestKit_TText, cant_go_prev_link_in_empty_text) {
	TText T;

	T.GoPrevLink();

	EXPECT_EQ(T.GetRetCode(), TextNoPrev);
}

TEST(TestKit_TText, can_get_line) {
	TStr str = "Test";

	TTextLink Link(str);
	PTTextLink pLink = &Link;
		
	TText T(pLink);
	
	EXPECT_EQ(T.GetLine(), str);
}

TEST(TestKit_TText, can_set_line) {
	TStr str = "Test";
	TStr expected_str = "Expected";

	TTextLink Link = TTextLink(str);
	PTTextLink pLink = &Link;
	
	TText T(pLink);	
	T.SetLine(expected_str);
	
	EXPECT_EQ(T.GetLine(), expected_str);
}

TEST(TestKit_TText, can_insert_next_line) {
	TStr str = "Test";
	TStr expected_str = "Expected";

	TTextLink Link(str);
	PTTextLink pLink = &Link;
	
	TTextLink::InitMemSystem(1);
	TText T(pLink);
	T.InsNextLine(expected_str);
	T.GoNextLink();

	EXPECT_EQ(T.GetLine(), expected_str);
}

TEST(TestKit_TText, can_insert_next_section) {
	TStr str = "Test";
	TStr expected_str = "Expected";

	TTextLink Link(str);
	PTTextLink pLink = &Link;

	TTextLink::InitMemSystem(1);
	TText T(pLink);
	T.InsNextSection(expected_str);
	T.GoNextLink();

	EXPECT_EQ(T.GetLine(), expected_str);
}

TEST(TestKit_TText, can_insert_down_line) {
	TStr str = "Test";
	TStr expected_str = "Expected";

	TTextLink Link(str);
	PTTextLink pLink = &Link;
	
	TTextLink::InitMemSystem(1);
	TText T(pLink);
	T.InsDownLine(expected_str);
	T.GoDownLink();

	EXPECT_EQ(T.GetLine(), expected_str);
}

TEST(TestKit_TText, can_insert_down_section) {
	TStr str = "Test";
	TStr expected_str = "Expected";

	TTextLink Link(str);
	PTTextLink pLink = &Link;

	TTextLink::InitMemSystem(1);
	TText T(pLink);
	T.InsDownSection(expected_str);
	T.GoDownLink();

	EXPECT_EQ(T.GetLine(), expected_str);
}

TEST(TestKit_TText, cant_insert_next_line_in_full_text) {
	TStr str = "Test";
	TStr expected_str = "Expected";

	TTextLink Link(str);
	PTTextLink pLink = &Link;
		
	TText T(pLink);
	T.InsNextLine(expected_str);
	T.GoNextLink();

	EXPECT_EQ(T.GetRetCode(), TextError);
}

TEST(TestKit_TText, cant_insert_next_section_in_full_text) {
	TStr str = "Test";
	TStr expected_str = "Expected";

	TTextLink Link(str);
	PTTextLink pLink = &Link;

	TText T(pLink);
	T.InsNextSection(expected_str);
	T.GoNextLink();

	EXPECT_EQ(T.GetRetCode(), TextError);
}

TEST(TestKit_TText, can_delete_next_line) {
	TStr str = "Test";
	TStr expected_str = "Expected";

	TTextLink Link(str);
	PTTextLink pLink = &Link;

	TTextLink::InitMemSystem(1);
	TText T(pLink);
	T.InsNextLine(expected_str);	
	T.DelNextLine();
	T.GoNextLink();

	EXPECT_EQ(T.GetRetCode(), TextError);
}

TEST(TestKit_TText, can_delete_next_section) {
	TStr str = "Test";
	TStr expected_str = "Expected";

	TTextLink Link(str);
	PTTextLink pLink = &Link;

	TTextLink::InitMemSystem(1);
	TText T(pLink);
	T.InsNextSection(expected_str);
	T.DelNextSection();
	T.GoNextLink();

	EXPECT_EQ(T.GetRetCode(), TextError);
}

TEST(TestKit_TText, can_delete_down_line) {
	TStr str = "Test";
	TStr expected_str = "Expected";

	TTextLink Link(str);
	PTTextLink pLink = &Link;

	TTextLink::InitMemSystem(1);
	TText T(pLink);
	T.InsDownLine(expected_str);
	T.DelDownLine();

	EXPECT_EQ(T.GetRetCode(), TextOK);
}

TEST(TestKit_TText, can_delete_down_section) {
	TStr str = "Test";
	TStr expected_str = "Expected";

	TTextLink Link(str);
	PTTextLink pLink = &Link;

	TTextLink::InitMemSystem(1);
	TText T(pLink);
	T.InsDownSection(expected_str);
	T.DelDownSection();

	EXPECT_EQ(T.GetRetCode(), TextOK);
}

TEST(TestKit_TText, cant_delete_next_line_in_empty_text) {
	TTextLink::InitMemSystem(1);
	TText T;

	T.DelNextLine();	

	EXPECT_EQ(T.GetRetCode(), TextNoNext);
}

TEST(TestKit_TText, cant_delete_next_section_in_empty_text) {
	TTextLink::InitMemSystem(1);
	TText T;

	T.DelNextSection();

	EXPECT_EQ(T.GetRetCode(), TextNoNext);
}

TEST(TestKit_TText, cant_delete_down_line_in_empty_text) {	
	TText T;	

	T.DelDownLine();

	EXPECT_EQ(T.GetRetCode(), TextError);
}

TEST(TestKit_TText, cant_delete_down_section_in_empty_text) {
	TText T;

	T.DelDownLine();

	EXPECT_EQ(T.GetRetCode(), TextError);
}
