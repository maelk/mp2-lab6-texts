# Отчёт по выполненной работе "Структура хранения текстов"
## Введение

**Цель данной работы** — разработка учебного редактора текстов с поддержкой следующих операций:

* выбор текста для редактирования (или создание нового текста);
* демонстрация текста на экране дисплея;
* поддержка средств указания элементов (уровней) текста;
* вставка, удаление и замена строк текста;
* запись подготовленного текста в файл.

Была предложена следующая структура хранения текста:

![](classes.png)

Она подразумевает реализацию двух основных классов, `TTextLink` для хранения строк, и `TText` для хранения собственно текста.

Более точное описание структуры текста:

* каждое звено структуры хранения содержит два поля указателей и поле для хранения данных;
* нижний уровень иерархической структуры звеньев ограничивается
* уровнем строк, а не символов. Это повышает эффективность использования памяти, так как в случае отдельного уровня для хранения символов, затраты на хранение служебной информации (указатели) в несколько раз превышают затраты на хранение самих данных.При использовании строк в качестве атомарных элементов поле для хранения данных является массивом символов заданного размера;
* количество уровней и количество звеньев на каждом уровне может быть произвольным;
* при использовании звена на атомарном уровне поле указателя на нижний уровень структуры устанавливается равным значению NULL;
* при использовании звена на структурных уровнях представления текста поле для хранения строки текста используется для размещения наименования соответствующего элемента текста;
* количество используемых уровней представления в разных фрагментах текста может быть различным.


> **Примечание**. Далее приведены тексты файлов не целиком, а только лишь описания и реализации классов.

## Реализация класса "Строка текста"
Класс, реализующий строку текста `TTextLink`, наследует от базового абстрактного класса `TDatValue`.

### Класс TDatValue - абстрактный класс для объектов-значений списка
```C++
class TDatValue {
public:
    virtual TDatValue* getCopy() = 0;
    ~TDatValue() {};
};
```

В данном классе реализовано не только очевидное хранение данных текущей строки, но также алгоритм сборки мусора с неразрывно связанной специальной системой выделения памяти.

Алгоритм сборки мусора состоит из трех этапов.
1. Обход текста и маркирование звеньев текста специальными символами (например, «&&&»).
2. Обход и маркирование списка свободных звеньев.
3. Проход по всему непрерывному участку памяти как по непрерывному набору звеньев. Если звено промаркировано, то маркер снимается. В противном случае, найдено звено, на которое нет ссылок («мусор»), и это звено возвращается в список свободных звеньев.

Ещё одна сложность также состоит в том, что нам приходится по-особому выделять память для новых строк, а значит, нужно перегружать операторы `new` и `delete`.

Для выделения памяти под звено перегружается оператор `new`, который выделяет новое звено из списка свободных звеньев. При освобождении звена в перегруженном операторе `delete` происходит возвращение памяти в список свободных звеньев.

### Класс TTextMem для управления памятью и TTextLink для хранения строк текста
```C++
class TText;
class TTextLink;

class TTextMem {
public:
    TTextLink* pFirst; // указатель на первое звено
    TTextLink* pLast;  // указатель на последнее звено
    TTextLink* pFree;  // указатель на первое свободное звено

    friend class TTextLink;
};

class TTextLink : public TDatValue {
public:
    // Методы для управления памятью
    static void InitMemSystem(size_t size = MEMORY_SIZE); // инициализация памяти
    static void PrintFreeLink(void);     // печать свободных звеньев
    static void MemCleaner(TText& text); // сборка мусора
    void* operator new(size_t size);     // выделение звена
    void operator delete(void* pM);      // освобождение звена

    // Методы для работы со звеном текста
    TTextLink(std::string s = "", TTextLink* pn = nullptr, TTextLink* pd = nullptr);
    ~TTextLink(void) {}
    int IsAtom(void); // проверка атомарности звена
    TTextLink* GetNext(void);
    TTextLink* GetDown(void);
    TDatValue* GetCopy(void);

protected:
    std::string str_; // поле для хранения строки текста
    TTextLink *pNext_, *pDown_; // указатели по тек. уровень и на подуровень
    static TTextMem memHeader_; // система управления памятью

    friend std::ostream& operator<<(std::ostream &os, TTextLink &tm);
    friend class TText;
};
```

```C++
TTextMem TTextLink::memHeader_;

//
// Методы для управления памятью
//

// Инициализация памяти
void TTextLink::InitMemSystem(size_t size) {
    memHeader_.pFirst = new TTextLink[size];
    memHeader_.pFree = memHeader_.pFirst;
    memHeader_.pLast = memHeader_.pFirst + (size - 1);

    TTextLink* pLink = memHeader_.pFirst;
    for (size_t i = 0; i < size - 1; i++, pLink++) {
        pLink->pNext_ = pLink + 1;
    }
    pLink->pNext_ = nullptr;
}

// Печать свободных звеньев
void TTextLink::PrintFreeLink() {
    TTextLink* pLink = memHeader_.pFree;
    std::cout << "List of free links:" << std::endl;
    while (pLink != nullptr) {
        std::cout << pLink->str_ << std::endl;
        pLink = pLink->pNext_;
    }
}

// Выделение звена
void* TTextLink::operator new(size_t size) {
    TTextLink* pLink = memHeader_.pFree;
    if (memHeader_.pFree != nullptr)
        memHeader_.pFree = pLink->pNext_;
    return pLink;
}

// Освобождение звена
void TTextLink::operator delete(void* pM) {
    TTextLink *pLink = (TTextLink*)pM;
    pLink->pNext_ = memHeader_.pFree;
    memHeader_.pFree = pLink;
}

// Сборка мусора
void TTextLink::MemCleaner(TText& text) {
    for (text.Reset(); !text.IsTextEnded(); text.GoNext()) {
        if (text.GetLine().find("&&&") != 0)
            text.SetLine("&&&" + text.GetLine());
    }

    TTextLink* pLink = memHeader_.pFree;
    for (; pLink != nullptr; pLink = pLink->pNext_)
        pLink->str_ = "&&&" + pLink->str_;

    pLink = memHeader_.pFirst;
    for (; pLink <= memHeader_.pLast; pLink++) {
        if (pLink->str_.find("&&&"))
            pLink->str_.erase(0, 3);
        else
            delete pLink;
    }
}

//
// Методы для работы со звеном текста
//

TTextLink::TTextLink(std::string s, TTextLink* pn, TTextLink* pd) {
    pNext_ = pn;
    pDown_ = pd;
    str_ = s;
}

int TTextLink::IsAtom() {
    return pDown_ == nullptr;
}

TTextLink* TTextLink::GetNext() {
    return pNext_;
}

TTextLink* TTextLink::GetDown() {
    return pDown_;
}

TDatValue* TTextLink::GetCopy() {
    return new TTextLink(str_, pNext_, pDown_);
}

std::ostream& operator<<(std::ostream& os, TTextLink& tm) {
    os << tm.str_;
    return os;
}
```

## Реализация класса "Текст"

Сложность реализации данного класса состоит в большом количестве необходимых для работы с текстом методов. Это и вставка и удаление текста на текущем и подуровне, итерирование текста, печать, ввод и вывод, связанный с файлами, а также копирование.

Для копирования текста необходимо осуществить обход текста. Так как структура текста является нелинейной, то копирование производится за два прохода, при этом для навигации по исходному тексту и тексту копии используется один объединенный стек.

Первый проход производится при подъеме на строку из подуровня – для текущей строки выполняется:

* создание копии звена;
* заполнение в звене-копии поля указателя подуровня pDown (подуровень уже скопирован);
* запись в звене-копии в поле данных значения “Copy”, используемое как маркер для распознавания звена при попадании на него при втором проходе; предполагается, что в тексте данный маркер не встречается;
* запись в звене-копии в поле указателя следующего звена pNext указателя на звено-оригинал (для возможности последующего копирования текста исходной строки);
* запись указателя на звено-копию в стек. Второй проход производится при извлечении звена-копии из стека (распознается по маркеру “Copy”)– в этом случае необходимо выполнить:
* заполнение в звене-копии полей данных и указателя следующего звена;
* указатель на звено-копию запоминается в служебной переменной.

### Класс TText
```C++
static int textLevel; // номер текущего уровня текста

TText::TText(TTextLink* pl) {
    if (pl == nullptr)
        pl = new TTextLink();
    pFirst_ = pl;
    GoFirstLink();
}

TText::~TText() {
    pFirst_ = nullptr;
}

//
// Копирование текста
//

// Поиск первого атома
TTextLink* TText::GetFirstAtom(TTextLink* pl) {
    TTextLink* tmp = pl;

    while (!tmp->IsAtom()) {
        st_.push(tmp);
        tmp = tmp->GetDown();
    }

    return tmp;
}

TText* TText::GetCopy() {
    TTextLink *pl1, *pl2, *pl = pFirst_, *cpl = nullptr;

    if (pFirst_ != nullptr) {
        while (!st_.empty())
            st_.pop();

        while (true) {
            if (pl != nullptr) {
                pl = GetFirstAtom(pl);
                st_.push(pl);
                pl = pl->GetDown();
            } else if (st_.empty()) {
                break;
            } else {
                pl1 = st_.top();
                st_.pop();

                if (pl1->str_ != "Copy") {
                    pl2 = new TTextLink("Copy", pl1, cpl);
                    st_.push(pl2);
                    pl = pl1->GetNext();
                    cpl = nullptr;
                } else {
                    pl1->str_ = pl1->pNext_->str_;
                    pl1->pNext_ = cpl;
                    cpl = pl1;
                }
            }
        }
    }

    return new TText(cpl);
}

//
// Навигация
//

// Переход к первой строке
void TText::GoFirstLink() {
    while (!path_.empty())
        path_.pop();
    pCurrent_ = pFirst_;
}

// Переход к следующей строке по Down
void TText::GoDownLink() {
    if (pCurrent_ != nullptr)
        if (pCurrent_->pDown_ != nullptr) {
            path_.push(pCurrent_);
            pCurrent_ = pCurrent_->pDown_;
        }
}

// Переход к следующей строке по Next
void TText::GoNextLink() {
    if (pCurrent_ != nullptr)
        if (pCurrent_->pNext_ != nullptr) {
            path_.push(pCurrent_);
            pCurrent_ = pCurrent_->pNext_;
        }
}

// Переход к предыдущей позиции в тексте
void TText::GoPrevLink() {
    if (!path_.empty()) {
        pCurrent_ = path_.top();
        path_.pop();
    }
}

//
// Доступ
//

// Чтение текущей строки
std::string TText::GetLine() {
    if (pCurrent_ == nullptr)
        return "";
    else
        return pCurrent_->str_;

}

// Замена текущей строки
void TText::SetLine(std::string s) {
    if (pCurrent_ != nullptr)
        pCurrent_->str_ = s;
}

//
// Модификация
//

// Вставка строки в подуровень
void TText::InsDownLine(std::string s) {
    if (pCurrent_ != nullptr) {
        TTextLink* pd = pCurrent_->pDown_;
        TTextLink* pl = new TTextLink(s, pd, nullptr);
        pCurrent_->pDown_ = pl;
    }
}

// Вставка раздела в подуровень
void TText::InsDownSection(std::string s) {
    if (pCurrent_ != nullptr) {
        TTextLink* pd = pCurrent_->pDown_;
        TTextLink* pl = new TTextLink(s, nullptr, pd);
        pCurrent_->pDown_ = pl;
    }
}

// Вставка строки в том же уровне
void TText::InsNextLine(std::string s) {
    if (pCurrent_ != nullptr) {
        TTextLink* pn = pCurrent_->pNext_;
        TTextLink* pl = new TTextLink(s, pn, nullptr);
        pCurrent_->pDown_ = pl;
    }
}

// Вставка раздела в том же уровне
void TText::InsNextSection(std::string s) {
    if (pCurrent_ != nullptr) {
        TTextLink* pn = pCurrent_->pNext_;
        TTextLink* pl = new TTextLink(s, nullptr, pn);
        pCurrent_->pDown_ = pl;
    }
}

// Удаление строки в подуровне
void TText::DelDownLine() {
    if (pCurrent_ != nullptr)
        if (pCurrent_->pDown_ != nullptr) {
            TTextLink* pl1 = pCurrent_->pDown_;
            TTextLink* pl2 = pl1->pNext_;
            if (pl1->pDown_ == nullptr)
                pCurrent_->pDown_ = pl2;
        }
}

// Удаление раздела в подуровне
void TText::DelDownSection() {
    if (pCurrent_ != nullptr)
        if (pCurrent_->pDown_ != nullptr) {
            TTextLink* pl1 = pCurrent_->pDown_;
            TTextLink* pl2 = pl1->pNext_;
            pCurrent_->pDown_ = pl2;
        }
}

// Удаление строки в том же уровне
void TText::DelNextLine() {
    if (pCurrent_ != nullptr)
        if (pCurrent_->pNext_ != nullptr) {
            TTextLink* pl1 = pCurrent_->pNext_;
            TTextLink* pl2 = pl1->pNext_;
            if (pl1->pDown_ == nullptr)
                pCurrent_->pNext_ = pl2;
        }
}

// Удаление раздела в том же уровне
void TText::DelNextSection() {
    if (pCurrent_ != nullptr)
        if (pCurrent_->pNext_ != nullptr) {
            TTextLink* pl1 = pCurrent_->pNext_;
            TTextLink* pl2 = pl1->pNext_;
            pCurrent_->pNext_ = pl2;
        }
}

//
// Итератор
//

// Установить на первую запись
void TText::Reset() {
    while (!st_.empty())
        st_.pop();

    pCurrent_ = pFirst_;
    if (pCurrent_ != nullptr) {
        st_.push(pCurrent_);
        if (pCurrent_->pNext_ != nullptr)
            st_.push(pCurrent_->pNext_);
        if (pCurrent_->pDown_ != nullptr)
            st_.push(pCurrent_->pDown_);
    }
}

// Текст завершён?
bool TText::IsTextEnded() const {
    return !st_.size();
}

// Переход к следующей записи
void TText::GoNext() {
    if (!IsTextEnded()) {
        pCurrent_ = st_.top();
        st_.pop();
        if (pCurrent_ != pFirst_) {
            if (pCurrent_->pNext_ != nullptr)
                st_.push(pCurrent_->pNext_);
            if (pCurrent_->pDown_ != nullptr)
                st_.push(pCurrent_->pDown_);
        }
    }
}

//
// Работа с файлами
//

// Ввод текста из файла
void TText::Read(char* pFileName) {
    std::ifstream txtFile(pFileName);

    if (txtFile)
        pFirst_ = ReadText(txtFile);
}

// Вывод текста в файл
void TText::Write(char* pFileName) {
    textLevel = 0;
    std::ofstream textFile(pFileName);

    PrintTextF(pFirst_, textFile);
}

//
// Печать
//

// Печать текста
void TText::Print() {
    textLevel = 0;
    PrintText(pFirst_);
}

// Печать текста со звена ptl
void TText::PrintText(TTextLink* ptl) {
    if (ptl != nullptr) {
        for (int i = 0; i < textLevel; i++)
            std::cout << "  ";
        std::cout << " " << *ptl << std::endl;
        textLevel++;
        PrintText(ptl->GetDown());
        textLevel--;
        PrintText(ptl->GetNext());
    }
}

// Печать текста со звена ptl в файл
void TText::PrintTextF(TTextLink *ptl, std::ofstream& textFile) {
    if (ptl != nullptr) {
        for (int i = 0; i < textLevel; i++)
            textFile << ' ';
        textFile << ' ' << ptl->str_ << std::endl;
        textLevel++;
        PrintTextF(ptl->GetDown(), textFile);
        textLevel--;
        PrintTextF(ptl->GetNext(), textFile);
    }
}

// Чтение текста из файла
TTextLink* TText::ReadText(std::ifstream& txtFile) {
    std::string buf;
    TTextLink *ptl, *pHead;
    pHead = ptl = new TTextLink();

    while (!txtFile.eof()) {
        getline(txtFile, buf);

        if (buf[0] == '}')
            break;
        else if (buf[0] == '{')
            ptl->pDown_ = ReadText(txtFile);
        else {
            ptl->pNext_ = new TTextLink(buf);
            ptl = ptl->pNext_;
        }
    }
    ptl = pHead;
    if (pHead->pDown_ == nullptr) {
        pHead = pHead->pNext_;
        delete ptl;
    }
    return pHead;
}
```

```C++
#include "TText.h"

static int textLevel; // номер текущего уровня текста

TText::TText(TTextLink* pl) {
    if (pl == nullptr)
        pl = new TTextLink();
    pFirst_ = pl;
    GoFirstLink();
}

TText::~TText() {
    pFirst_ = nullptr;
}

//
// Копирование текста
//

// Поиск первого атома
TTextLink* TText::GetFirstAtom(TTextLink* pl) {
    TTextLink* tmp = pl;

    while (!tmp->IsAtom()) {
        st_.push(tmp);
        tmp = tmp->GetDown();
    }

    return tmp;
}

TText* TText::GetCopy() {
    //TODO
    return nullptr;
}

//
// Навигация
//

// Переход к первой строке
void TText::GoFirstLink() {
    while (!path_.empty())
        path_.pop();
    pCurrent_ = pFirst_;
}

// Переход к следующей строке по Down
void TText::GoDownLink() {
    if (pCurrent_ != nullptr)
        if (pCurrent_->pDown_ != nullptr) {
            path_.push(pCurrent_);
            pCurrent_ = pCurrent_->pDown_;
        }
}

// Переход к следующей строке по Next
void TText::GoNextLink() {
    if (pCurrent_ != nullptr)
        if (pCurrent_->pNext_ != nullptr) {
            path_.push(pCurrent_);
            pCurrent_ = pCurrent_->pNext_;
        }
}

// Переход к предыдущей позиции в тексте
void TText::GoPrevLink() {
    if (!path_.empty()) {
        pCurrent_ = path_.top();
        path_.pop();
    }
}

//
// Доступ
//

// Чтение текущей строки
std::string TText::GetLine() {
    if (pCurrent_ == nullptr)
        return "";
    else
        return pCurrent_->str_;

}

// Замена текущей строки
void TText::SetLine(std::string s) {
    if (pCurrent_ != nullptr)
        pCurrent_->str_ = s;
}

//
// Модификация
//

// Вставка строки в подуровень
void TText::InsDownLine(std::string s) {
    if (pCurrent_ != nullptr) {
        TTextLink* pd = pCurrent_->pDown_;
        TTextLink* pl = new TTextLink(s, pd, nullptr);
        pCurrent_->pDown_ = pl;
    }
}

// Вставка раздела в подуровень
void TText::InsDownSection(std::string s) {
    if (pCurrent_ != nullptr) {
        TTextLink* pd = pCurrent_->pDown_;
        TTextLink* pl = new TTextLink(s, nullptr, pd);
        pCurrent_->pDown_ = pl;
    }
}

// Вставка строки в том же уровне
void TText::InsNextLine(std::string s) {
    if (pCurrent_ != nullptr) {
        TTextLink* pn = pCurrent_->pNext_;
        TTextLink* pl = new TTextLink(s, pn, nullptr);
        pCurrent_->pDown_ = pl;
    }
}

// Вставка раздела в том же уровне
void TText::InsNextSection(std::string s) {
    if (pCurrent_ != nullptr) {
        TTextLink* pn = pCurrent_->pNext_;
        TTextLink* pl = new TTextLink(s, nullptr, pn);
        pCurrent_->pDown_ = pl;
    }
}

// Удаление строки в подуровне
void TText::DelDownLine() {
    if (pCurrent_ != nullptr)
        if (pCurrent_->pDown_ != nullptr) {
            TTextLink* pl1 = pCurrent_->pDown_;
            TTextLink* pl2 = pl1->pNext_;
            if (pl1->pDown_ == nullptr)
                pCurrent_->pDown_ = pl2;
        }
}

// Удаление раздела в подуровне
void TText::DelDownSection() {
    if (pCurrent_ != nullptr)
        if (pCurrent_->pDown_ != nullptr) {
            TTextLink* pl1 = pCurrent_->pDown_;
            TTextLink* pl2 = pl1->pNext_;
            pCurrent_->pDown_ = pl2;
        }
}

// Удаление строки в том же уровне
void TText::DelNextLine() {
    if (pCurrent_ != nullptr)
        if (pCurrent_->pNext_ != nullptr) {
            TTextLink* pl1 = pCurrent_->pNext_;
            TTextLink* pl2 = pl1->pNext_;
            if (pl1->pDown_ == nullptr)
                pCurrent_->pNext_ = pl2;
        }
}

// Удаление раздела в том же уровне
void TText::DelNextSection() {
    if (pCurrent_ != nullptr)
        if (pCurrent_->pNext_ != nullptr) {
            TTextLink* pl1 = pCurrent_->pNext_;
            TTextLink* pl2 = pl1->pNext_;
            pCurrent_->pNext_ = pl2;
        }
}

//
// Итератор
//

// Установить на первую запись
void TText::Reset() {
    while (!st_.empty())
        st_.pop();

    pCurrent_ = pFirst_;
    if (pCurrent_ != nullptr) {
        st_.push(pCurrent_);
        if (pCurrent_->pNext_ != nullptr)
            st_.push(pCurrent_->pNext_);
        if (pCurrent_->pDown_ != nullptr)
            st_.push(pCurrent_->pDown_);
    }
}

// Текст завершён?
bool TText::IsTextEnded() const {
    return !st_.size();
}

// Переход к следующей записи
void TText::GoNext() {
    if (!IsTextEnded()) {
        pCurrent_ = st_.top();
        st_.pop();
        if (pCurrent_ != pFirst_) {
            if (pCurrent_->pNext_ != nullptr)
                st_.push(pCurrent_->pNext_);
            if (pCurrent_->pDown_ != nullptr)
                st_.push(pCurrent_->pDown_);
        }
    }
}

//
// Работа с файлами
//

// Ввод текста из файла
void TText::Read(char* pFileName) {
    std::ifstream txtFile(pFileName);

    if (txtFile)
        pFirst_ = ReadText(txtFile);
}

// Вывод текста в файл
void TText::Write(char* pFileName) {
    textLevel = 0;
    std::ofstream textFile(pFileName);

    PrintTextF(pFirst_, textFile);
}

//
// Печать
//

// Печать текста
void TText::Print() {
    textLevel = 0;
    PrintText(pFirst_);
}

// Печать текста со звена ptl
void TText::PrintText(TTextLink* ptl) {
    if (ptl != nullptr) {
        for (int i = 0; i < textLevel; i++)
            std::cout << "  ";
        std::cout << " " << *ptl << std::endl;
        textLevel++;
        PrintText(ptl->GetDown());
        textLevel--;
        PrintText(ptl->GetNext());
    }
}

// Печать текста со звена ptl в файл
void TText::PrintTextF(TTextLink *ptl, std::ofstream& textFile) {
    if (ptl != nullptr) {
        for (int i = 0; i < textLevel; i++)
            textFile << ' ';
        textFile << ' ' << ptl->str_ << std::endl;
        textLevel++;
        PrintTextF(ptl->GetDown(), textFile);
        textLevel--;
        PrintTextF(ptl->GetNext(), textFile);
    }
}

// Чтение текста из файла
TTextLink* TText::ReadText(std::ifstream& txtFile) {
    std::string buf;
    TTextLink *ptl, *pHead;
    pHead = ptl = new TTextLink();

    while (!txtFile.eof()) {
        getline(txtFile, buf);

        if (buf[0] == '}')
            break;
        else if (buf[0] == '{')
            ptl->pDown_ = ReadText(txtFile);
        else {
            ptl->pNext_ = new TTextLink(buf);
            ptl = ptl->pNext_;
        }
    }
    ptl = pHead;
    if (pHead->pDown_ == nullptr) {
        pHead = pHead->pNext_;
        delete ptl;
    }
    return pHead;
}
```

## Тесты Google Test для класса TText
```C++
#include <gtest/gtest.h>
#include "TText.h"

//
// Создание
//

TEST(TText, can_create_text_without_having_textlink) {
    // Act
    TTextLink::InitMemSystem();

    // Assert
    ASSERT_NO_THROW(TText text);
}

TEST(TText, can_create_text_without_having_textlink_specifying_memory) {
    // Act
    TTextLink::InitMemSystem(3);

    // Assert
    ASSERT_NO_THROW(TText text);
}

TEST(TText, can_create_text_from_textlink) {
    // Arrange
    TTextLink::InitMemSystem();
    TTextLink* link;

    // Act
    link = new TTextLink();

    // Assert
    ASSERT_NO_THROW(TText text(link));
}

TEST(TText, can_copy_text) {
    // Arrange & Act
    TTextLink::InitMemSystem();
    TText text;

    // Assert
    ASSERT_NO_THROW(text.GetCopy());
}

//
// Навигация
//

TEST(TText, can_go_first_link) {
    // Arrange & Act
    TTextLink::InitMemSystem();
    TText text;

    // Assert
    ASSERT_NO_THROW(text.GoFirstLink());
}

TEST(TText, can_go_down_link) {
    // Arrange & Act
    TTextLink::InitMemSystem();
    TText text;

    // Assert
    ASSERT_NO_THROW(text.GoDownLink());
}

TEST(TText, can_go_next_link) {
    // Arrange & Act
    TTextLink::InitMemSystem();
    TText text;

    // Assert
    ASSERT_NO_THROW(text.GoNextLink());
}

TEST(TText, can_go_prev_link) {
    // Arrange & Act
    TTextLink::InitMemSystem();
    TText text;

    // Assert
    ASSERT_NO_THROW(text.GoPrevLink());
}

//
// Доступ
//

TEST(TText, can_get_current_line) {
    // Arrange & Act
    TTextLink::InitMemSystem();
    TText text;

    // Assert
    ASSERT_NO_THROW(text.GetLine());
}

TEST(TText, can_set_current_line) {
    // Arrange
    TTextLink::InitMemSystem();
    TText text;
    std::string str;

    // Act
    str = "test";

    // Assert
    ASSERT_NO_THROW(text.SetLine(str));
}

//
// Модификация: вставка
//

TEST(TText, can_insert_line_down) {
    // Arrange
    TTextLink::InitMemSystem();
    TText text;
    std::string str;

    // Act
    str = "test";

    // Assert
    ASSERT_NO_THROW(text.InsDownLine(str));
}


TEST(TText, can_add_down_a_line) {
    // Arg
    TTextLink::InitMemSystem();
    TText text;
    std::string str;

    // Act
    str = "test";
    text.GoFirstLink();
    text.InsDownLine(str);
    text.GoDownLink();

    // Assert
    EXPECT_EQ(text.GetLine(), str);
}

TEST(TText, can_insert_section_down) {
    // Arrange
    TTextLink::InitMemSystem();
    TText text;
    std::string str;

    // Act
    str = "test";

    // Assert
    ASSERT_NO_THROW(text.InsDownSection(str));
}

TEST(TText, can_insert_line_next) {
    // Arrange
    TTextLink::InitMemSystem();
    TText text;
    std::string str;

    // Act
    str = "test";

    // Assert
    ASSERT_NO_THROW(text.InsNextLine(str));
}

TEST(TText, can_insert_section_next) {
    // Arrange
    TTextLink::InitMemSystem();
    TText text;
    std::string str;

    // Act
    str = "test";

    // Assert
    ASSERT_NO_THROW(text.InsNextSection(str));
}

//
// Модификация: удаление
//

TEST(TText, can_delete_line_down) {
    // Arrange
    TTextLink::InitMemSystem();
    TText text;

    // Act & Assert
    ASSERT_NO_THROW(text.DelDownLine());
}

TEST(TText, can_delete_section_down) {
    // Arrange
    TTextLink::InitMemSystem();
    TText text;

    // Act & Assert
    ASSERT_NO_THROW(text.DelDownSection());
}

TEST(TText, can_delete_line_next) {
    // Arrange
    TTextLink::InitMemSystem();
    TText text;

    // Act & Assert
    ASSERT_NO_THROW(text.DelNextLine());
}

TEST(TText, can_delete_section_next) {
    // Arrange
    TTextLink::InitMemSystem();
    TText text;

    // Act & Assert
    ASSERT_NO_THROW(text.DelNextSection());
}

//
// Итератор
//

TEST(TText, can_reset_iterator) {
    // Arrange
    TTextLink::InitMemSystem();
    TText text;

    // Act & Assert
    ASSERT_NO_THROW(text.Reset());
}

TEST(TText, can_check_if_text_ended) {
    // Arrange
    TTextLink::InitMemSystem();
    TText text;

    // Act & Assert
    ASSERT_NO_THROW(text.IsTextEnded());
}

TEST(TText, can_iterate) {
    // Arrange
    TTextLink::InitMemSystem();
    TText text;

    // Act & Assert
    ASSERT_NO_THROW(text.GoNext());
}

//
// Работа с файлами
//

TEST(TText, can_write_to_file) {
    // Arrange
    TTextLink::InitMemSystem();
    TText text;
    char* filename;

    // Act
    filename = "__test_text_out.txt";

    // Assert
    ASSERT_NO_THROW(text.Write(filename));
}

//
// Печать
//

TEST(TText, can_print_text) {
    // Arrange
    TTextLink::InitMemSystem();
    TText text;

    // Act & Assert
    ASSERT_NO_THROW(text.Print());
}
```

![](tests.png)

## Выводы
Был изучен и реализован один из способов представления структуры текста - очень важной сущности.
